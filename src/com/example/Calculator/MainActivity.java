package com.example.Calculator;



import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private TextView result = null;
	private String current_value = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		result = (TextView) findViewById(R.id.result);
		current_value = result.getText().toString();
	
		
		Button one = (Button) findViewById(R.id.one);
		one.setOnClickListener(this); 
		
		Button two = (Button) findViewById(R.id.two);
		two.setOnClickListener(this);
		
		Button three = (Button) findViewById(R.id.three);
		three.setOnClickListener(this);
		
		Button four = (Button) findViewById(R.id.four);
		four.setOnClickListener(this);
		
		Button five = (Button) findViewById(R.id.five);
		five.setOnClickListener(this);
		
		Button six = (Button) findViewById(R.id.six);
		six.setOnClickListener(this);
		
		Button seven = (Button) findViewById(R.id.seven);
		seven.setOnClickListener(this);
		
		Button eight = (Button) findViewById(R.id.eight);
		eight.setOnClickListener(this);
		
		Button nine = (Button) findViewById(R.id.nine);
		nine.setOnClickListener(this);
		
		Button zero = (Button) findViewById(R.id.zero);
		zero.setOnClickListener(this);
		
		Button plus = (Button) findViewById(R.id.plus);
		plus.setOnClickListener(this);
		
		Button minus = (Button) findViewById(R.id.minus);
		minus.setOnClickListener(this);
		
		Button divide = (Button) findViewById(R.id.divide);
		divide.setOnClickListener(this);
		
		Button multiply = (Button) findViewById(R.id.multiply);
		multiply.setOnClickListener(this);
		
		Button equals = (Button) findViewById(R.id.equals);
		equals.setOnClickListener(this);
		
		Button clear = (Button) findViewById(R.id.clear);
		clear.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		current_value = result.getText().toString();
		int num_vals = checkString(current_value);
		int view_id = v.getId();
		if(num_vals == 2 && (view_id == R.id.plus || view_id == R.id.minus || view_id == R.id.multiply || view_id == R.id.divide))
			doCalculation(current_value);
		current_value = result.getText().toString();
		
		if(current_value.equals("0"))
		{
			current_value = "";
		}
		
		switch (view_id) {
		case  R.id.one :
			result.setText(current_value + "1");
			break;
		case  R.id.two :
			result.setText(current_value + "2");
			break;
		case  R.id.three :
			result.setText(current_value + "3");
			break;
		case  R.id.four :
			result.setText(current_value + "4");
			break;
		case  R.id.five :
			result.setText(current_value + "5");
			break;
		case  R.id.six :
			result.setText(current_value + "6");
			break;
		case  R.id.seven :
			result.setText(current_value + "7");
			break;
		case  R.id.eight :
			result.setText(current_value + "8");
			break;
		case  R.id.nine :
			result.setText(current_value + "9");
			break;
		case  R.id.zero :
			result.setText(current_value + "0");
			break;
		case  R.id.plus :
			result.setText(current_value + "+");
			break;
		case  R.id.minus :
			result.setText(current_value + "-");
			break;
		case  R.id.multiply :
			result.setText(current_value + "x");
			break;
		case  R.id.divide :
			result.setText(current_value + "/");
			break;
		case  R.id.equals :
			doCalculation(current_value);
			break;
		case  R.id.clear :
			result.setText("");
			break;
		default:
			break;
		}
		
	}
	
	public void doCalculation(String current_value)
	{
		String[] split = current_value.split("x");
		int ans = 0;	
		if(split.length == 1)
		{
			split = current_value.split("/");
			if(split.length == 1)
			{
				split = current_value.split("\\+");
				if(split.length == 1)
				{
					split = current_value.split("-");
					if(split.length == 1)
					{
						ans = Integer.parseInt(current_value);
						Toast.makeText(MainActivity.this, "Choose + - / x", Toast.LENGTH_LONG).show();
					}
					else
					{
						ans = subtract(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
					}
				}
				else
				{
					ans = add(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
				}
			}
			else
			{
				ans = divide(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
			}
		}
		else
		{
			ans = multiply(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
		}
		
		if(ans == 0)
		{
			result.setText("0");
		}
		else
		{
			current_value = ans + "";
			result.setText(current_value);
		}
	
		
	}

	public int checkString(String current_value)
	{
		String[] split = current_value.split("x");	
		if(split.length == 1)
		{
			split = current_value.split("/");
			if(split.length == 1)
			{
				split = current_value.split("\\+");
				if(split.length == 1)
				{
					split = current_value.split("-");
					if(split.length == 1)
					{
						return 1;
					}
					else
					{
						return 2;
					}
				}
				else
				{
					return 2;
				}
			}
			else
			{
				return 2;
			}
		}	
		else
		{
			return 2;
		}
	}

	public int add(int value1,int value2)
	{
		int result = (int)(value1 + value2);
		return result;
	}
	
	public int subtract(int value1,int value2)
	{
		int result = (int)(value1 - value2);
		return result;
	}
	
	public int multiply(int value1,int value2)
	{
		int result = (int)(value1 * value2);
		return result;
	}
	
	public int divide(int value1,int value2)
	{
		if((value1 == 0)||(value2 == 0))
		{
			Toast.makeText(MainActivity.this, "Can't divide by zero!", Toast.LENGTH_LONG).show();
			int result = 0;
			return result;
		}
		else
		{
			int result = (int)(value1 / value2);
			return result;
		}
		
			
	}
	
	
}
